/*Теоретичне питання

1.Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

AJAX - це метод отримання або відправлення даних, тобто взаємодія із сервером без перезавантаження сторінки. 
Завдяки цьому зменшується час відгуку і веб-сторінка стає інтерактивнішою. 


Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості 
characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, 
назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
Необов'язкове завдання підвищеної складності
Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. 
Бажано знайти варіант на чистому CSS без використання JavaScript.
Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.*/

const fetchFilmsBtn = document.querySelector(".btn");
const filmsList = document.querySelector(".films-list");

fetchFilmsBtn.addEventListener("click", () => {
  fetchFilms()
    .then((films) => {
      renderFilms(films);
  return Promise.all(films.map(film => fetchCharacters(film.characters)));
})
  .then(charactersData => renderCharacters(charactersData))
    .catch((error) => console.log(error));
});

function fetchFilms() {
  return fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((response) => {
      if (!response.ok) {
        throw new Error(response.status);
      }
      return response.json();
    }
  );
}

function fetchCharacters(characterUrls) {
  return Promise.all(characterUrls.map(url =>
    fetch(url).then(response => response.json())));
}
function renderFilms(films) {
  const markup = films
    .map((film) => {
      return `<li>
          <p><b>Episode ID</b>: ${film.episodeId}</p>
          <p><b>Film title</b>: ${film.name}</p>
          <p><b>Summary</b>: ${film.openingCrawl}</p>
        </li>`;
    })
    .join("");
  filmsList.insertAdjacentHTML("beforeend", markup);
}

function renderCharacters(charactersData) {
  charactersData.forEach((characters, index) => {
    const filmItem = filmsList.children[index];
    const charactersMarkup = characters.map(character => `<p><b>Character</b>: ${character.name}</p>`)
      .join("");
    
    filmItem.insertAdjacentHTML("beforeend", `<div>${charactersMarkup}</div>`);
  });
}